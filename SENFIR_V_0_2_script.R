#############################################################################################
# SENFIR version 0.2 (30-06-2020)                                                           #
# Author: Johannes Balling (Wageningen University)                                          #
# Enables the opportunity to classify so called fire related forest cover losses utilizing  #
# multi-sensor datastreams (namely: Sentinel-1, Sentinel-2, Landsat and active fire alerts  #
# based on MODIS and VIIRs sensor)                                                          #
# So far included:                                                                          #
#  1. Detection of forest cover loss of optical (I) and radar (II) data based on the bayts  #
#     package developed by Reiche et al 2015 / 2018?                                        #
#  2. Calculation of the uncertainties of the detected forest cover losses                  #
#  3. Intersection of detected forest cover losses and active fire alerts                   #
#  4. Classification of 7 archetypes taking the dates of the detected forest cover losses   #
#     of the optical and radar data respectively in respect to the date of fire into        #
#     account                                                                               #
#  5. Result is a map of the so called fire-related forest cover losses - archetypes        #
#                                                                                           #
# Note: All layers are already clipped to the desired Area of interest and in the spatial   #
# resolution of the Landsat images. AOI is a small region in Riau (Indonesia)               #
#############################################################################################

#------------------------ Parameter that need to be defined --------------------------------#
# Please set the working directory to the folder "SENFIR" you have downloaded from github    # 
setwd("/path/to_the_folder_called/SENFIR/")

# !!!!!!!!!!!!!!!!!!!!!!! YOU MAY NOT CHANGE ANYTHING AFTER THIS  !!!!!!!!!!!!!!!!!!!!!!!!!!#

#############################################################################################
#------------------------------- Installing required packages ------------------------------#
require(devtools)
if(packageVersion('raster') != '2.9-23'){
  install_version("raster", version = "2.9-23", repos = "http://cran.us.r-project.org")
  library('raster')
}else{
  library('raster')
}
if(packageVersion('sp') != '1.3-1'){
  install_version("sp", version = "1.3-1", repos = "http://cran.us.r-project.org")
  library('sp')
}else{
  library('sp')
}
if(packageVersion('parallel') != '3.4.4'){
  install_version("parallel", version = "3.4.4", repos = "http://cran.us.r-project.org")
  library('parallel')
}else{
  library('parallel')
}
if(packageVersion('lubridate') != '1.7.4'){
  install_version("lubridate", version = "1.7.4", repos = "http://cran.us.r-project.org")
  library('lubridate')
}else{
  library('lubridate')
}
if(packageVersion('bfast') != '1.5.7'){
  install_version("bfast", version = "1.5.7", repos = "http://cran.us.r-project.org")
  library('bfast')
}else{
  library('bfast')
}
if(packageVersion('bfastSpatial') != '0.6.3'){
  install_github('loicdtx/bfastSpatial')
  library('bfastSpatial')
}else{
  library('bfastSpatial')
}
if(packageVersion('bfastSpatial') != '0.1'){
  install_github('jreiche/bayts')
  library('bayts')
}else{
  library('bayts')
}

###############################################################################################
#----------------------------------- 0.Input data --------------------------------------------#
###############################################################################################
# The input data consists of:
#   1. Sentinel-1 backscatter values - VV and VH polarization (GRD data, already pre-processed)
s1vv = brick("./01_data/01_Sentinel_1/Sentinel_1_VV.grd")
s1vh = brick("./01_data/01_Sentinel_1/Sentinel_1_VH.grd")
#   2. Sentinel-2 Normalized Burned Ratio (NBR) - Calculation: 
#                             NBR = (Band 8 - Band 12) / (Band 8 + Band 12)
s2nbr = brick("./01_data/02_Sentinel_2/Sentinel_2_NBR.grd")
#   3. Landsat-7/8 Normalized Burned Ratio (NBR) - Calculation:
#                 Landsat 7 - NBR = (Band 4 - Band 7) / (Band 4 + Band 7)
#                 Landsat 8 - NBR = (Band 5 - Band 7) / (Band 5 + Band 7)
lnbr = brick("./01_data/03_Landsat/Landsat_NBR.grd")
#   4. Raster layer containing active fire alerts from the sensors MODIS and Viirs
ras_fire = brick("./01_data/04_Active_Fire/MODIS_VIIRs_active_fire.grd")
#   5. Forest mask - '2000 Percent Tree Cover' from the University of Maryland
fmask = raster("./01_data/05_Ancillary_data/01_Forest_mask/Forest_mask.grd") 
#   6. Forest Cover Loss mask - 'Forest Cover Loss' (2000 - 2018) fromt he University of Maryland 
flmask = raster("./01_data/05_Ancillary_data/02_Forest_cover_loss/Forest_cover_loss_mask.grd")

###############################################################################################
#----------------------- 1.Preparing input data for further analysis -------------------------#
# Includes the subset of pixels showing a forest cover of at least 75% (based on Hansen forest#
# cover map), excluding pixels showing forest disturbances during the training period from    #
# 2015 - 2017 (based on Hansen forest cover loss map) and the extraction of dates based on    #
# the layer names                                                                             #
###############################################################################################
# Preparing Sentinel-1 VH
lnbr_date = vector(mode="character", length=nlayers(lnbr))
for (i in 1:nlayers(lnbr)) {
  lnbr_date[i] = gsub("Date_","",lnbr[[i]]@data@names)
  lnbr[[i]][fmask < 75] = NA
  lnbr[[i]][flmask >= 15 &  flmask <= 17] = NA
}
lnbr_date = as.Date(lnbr_date, format = "%Y%m%d")

# Preparing Sentinel-1 VV
s1vv_date = vector(mode="character", length=nlayers(s1vv))
for (i in 1:nlayers(s1vv)) {
  s1vv_date[i] = gsub("Date_","",s1vv[[i]]@data@names)
  s1vv[[i]][fmask < 75] = NA
  s1vv[[i]][flmask >= 15 &  flmask <= 17] = NA
}
s1vv_date = as.Date(s1vv_date, format = "%Y%m%d")

# Preparing Sentinel-1 VH
s1vh_date = vector(mode="character", length=nlayers(s1vh))
for (i in 1:nlayers(s1vh)) {
  s1vh_date[i] = gsub("Date_","",s1vh[[i]]@data@names)
  s1vh[[i]][fmask < 75] = NA
  s1vh[[i]][flmask >= 15 &  flmask <= 17] = NA
}
s1vh_date = as.Date(s1vh_date, format = "%Y%m%d")

# Preparing Sentinel-2 NBR
s2nbr_date = vector(mode="character", length=nlayers(s2nbr))
for (i in 1:nlayers(s2nbr)) {
  s2nbr_date[i] = gsub("Date_","",s2nbr[[i]]@data@names)
  s2nbr[[i]][fmask < 75] = NA
  s2nbr[[i]][flmask >= 15 &  flmask <= 17] = NA
}
s2nbr_date = as.Date(s2nbr_date, format = "%Y%m%d")

# Preparing Active fire alerts
ras_fire_date = vector(mode="character", length=nlayers(ras_fire))
for (i in 1:nlayers(ras_fire)) {
  ras_fire_date[i] = gsub("Date_","",ras_fire[[i]]@data@names)
  ras_fire[[i]][fmask < 75] = NA
}
ras_fire_date = as.Date(ras_fire_date, format = "%Y_%m_%d")

#########################################################################################
#------------------------------ 2. Forest cover loss detection -------------------------#  
# The forest cover loss detection is based on a probabilistic machine learning approach #
# developed by Reiche et al 2015 (https://www.mdpi.com/2072-4292/7/5/4973). Published as#
# Bayts package on Git Hub (https://github.com/jreiche/bayts).                          #
#########################################################################################
# Number of cores used for parallelizing the calculation - Usage of all available cores #
cores = detectCores()
#--------------- 2.1 - Detection of forest cover loss based on Radar data -------------#
# Input parameter
# a. List of raster bricks
bL = list(s1vh,s1vv)
# b. dates of the raster bricks - here list of both dates for Sentinel-1 VV and VH
datesL = list(s1vh_date,s1vv_date)
# c. formular that is used for deseasonalizing the data (Here no deseasonalization)
formula = "response ~ 1"
# d. Modulation of the sd of Forest (F) and Non-Forest (NF) - sd(F),mean(NF),sd(NF)
pdfsdL = list(c(2,-4,2),c(2,-4,2))
# e. Start date of monitoring
start = 2018
# f. Threshold of probability at which non forest is flagged
PNF_min = 0.3
# g. Theshold of deforestation probability at which flagged change is confirmed
chi = 0.7
# Calculate Radar deforestation based on Sentinel-1 VV and Sentinel-1 VH
def_radar = baytsDDSpatial(bL = bL, datesL = datesL, formulaL = list(formula,formula), pdfsdL = pdfsdL,
                           start=start, PNFmin = PNF_min, chi = chi, mc.cores = cores)
# Subset the results on to the flagged date
def_radar = def_radar[[2]]
writeRaster(def_radar, filename = "./02_results/radar_forest_cover_loss.tif", format = "GTiff" ,overwrite=T)


#------------- 2.2 - Detection of forest cover loss based on Optical data ----------------#
# Input parameter
# a. List of raster bricks
bL = list(s2nbr, lnbr)
# b. dates of the raster bricks - here list of both dates for Sentinel-1 VV and VH
datesL = list(s2nbr_date, lnbr_date)
# c. formular that is used for deseasonalizing the data (Here no deseasonalization)
formula = "response ~ 1"
# d. Modulation of the sd of Forest (F) and Non-Forest (NF) - sd(F),mean(NF),sd(NF)
pdfsdL = list(c(2,-4,2),c(2,-4,2))
# e. Start date of monitoring
start = 2018
# f. Threshold of probability at which non forest is flagged
PNF_min = 0.3
# g. Theshold of deforestation probability at which flagged change is confirmed
chi = 0.7
# Calculate Optical deforestation based on Sentinel-2 NBR and Landsat-7/8 NBR
def_optical = baytsDDSpatial(bL = bL, datesL = datesL, formulaL = list(formula,formula), pdfsdL = pdfsdL,
                           start=start, PNFmin = PNF_min, chi = chi, mc.cores = cores)
# Subset the results on to the flagged date
def_optical = def_optical[[2]]
writeRaster(def_optical, filename = "./02_results/optical_forest_cover_loss.tif", format = "GTiff" ,overwrite=T)



#########################################################################################################
#----------------------- 3. Improving uncertainties of detected forest cover loss ----------------------#
#-------------------------- and inital intersection of fire and forest cover loss ----------------------#
#########################################################################################################
# Converting rasters of detected forest cover loss - radar data (i) and optical data (ii) and raster
# of the fire alerts into one matrix
stack_attemp  = stack(def_radar,def_optical,ras_fire)
fin_matrix = matrix(data=NA,nrow = ncell(stack_attemp[[1]]), ncol =  (5 + nlayers(stack_attemp)))
fin_matrix[,4] = as.vector(row(as.matrix(stack_attemp[[1]])))
fin_matrix[,5] = as.vector(col(as.matrix(stack_attemp[[1]])))
fin_matrix[,6] = as.vector(as.matrix(stack_attemp[[1]]))
fin_matrix[,7] = as.vector(as.matrix(stack_attemp[[2]]))
# Add active fire alerts to the matrix
for (i in 3:nlayers(stack_attemp)) {
  temp = as.character(as.vector(as.matrix(stack_attemp[[i]])))
  temp[temp == "0"] = NA
  temp[temp == "1"] = gsub("Date_","",stack_attemp[[i]]@data@names)
  temp = gsub("_","-",temp)
  fin_matrix[,i+5] = temp
}

#-------------------------- 3.1 Radar improvement and intersection ---------------------------------------#
# Subset further analysis based on the existence of radar detected forest cover loss in matrix (saves time)
rows = which(!is.na(fin_matrix[,6]), arr.ind=TRUE)
fin_matrix[,2] = as.numeric(fin_matrix[,2])
s1vv_date_temp = decimal_date(s1vv_date)
s1vv_date_temp = round(as.numeric(s1vv_date_temp), digits = 3)

# Improving detected forest cover loss by changing flagged forest cover loss date to date between detected
# forest cover loss and the last valid observation
# Creating initial classes for the temporal relationship of fire and forest cover loss based on whether forest
# cover loss was detected within a timespan of 30 days before and after a fire alert
# Initial classes:  1 -     Fire happened before the loss of forest cover
#                   10 -    Fire happened during the loss of forest cover
#                   100 -   Fire happened after the loss of forest cover
#                   1000 -  Forest cover loss was detected

for (i in 1:length(rows)) {
  temp_result = round(as.numeric(fin_matrix[rows[i],6]), digits = 3)
  if(unique(s1vv_date_temp %in% temp_result)[2] == T){
    fin_matrix[rows[i],6] = (temp_result + s1vv_date_temp[which(s1vv_date_temp %in% temp_result)-1] )/2
    fin_matrix[rows[i],6] = format(date_decimal(as.numeric(fin_matrix[rows[i],6])), "%Y-%m-%d")
  }

  fire_sequence = na.omit(as.Date(fin_matrix[rows[i],c(8:ncol(fin_matrix))],format = "%Y-%m-%d")) 
  fin_matrix[rows[i],2] = 1000
  if (any(fire_sequence >= (as.Date(fin_matrix[rows[i],6], format = "%Y-%m-%d") -30) & fire_sequence <= (as.Date(fin_matrix[rows[i],6], format = "%Y-%m-%d") +30)) == T) {
    fin_matrix[rows[i],2] = as.numeric(fin_matrix[rows[i],2]) + 10
    next
  }
  if (any(fire_sequence < (as.Date(fin_matrix[rows[i],6], format = "%Y-%m-%d") -30)) == T) {
    fin_matrix[rows[i],2] = as.numeric(fin_matrix[rows[i],2]) + 100
  }
  if (any(fire_sequence > (as.Date(fin_matrix[rows[i],6], format = "%Y-%m-%d") +30)) == T) {
    fin_matrix[rows[i],2] = as.numeric(fin_matrix[rows[i],2]) + 1
  }
}




#-------------------------- 3.2 Optical improvement and intersection ---------------------------------------#
# Subset further analysis based on the existence of radar detected forest cover loss in matrix (saves time)
rows = which(!is.na(fin_matrix[,7]), arr.ind=TRUE)
fin_matrix[,3] = as.numeric(fin_matrix[,3])

# Improving detected forest cover loss by changing flagged forest cover loss date to date between detected
# forest cover loss and the last valid observation
# Improving detected forest cover loss by changing flagged forest cover loss date to date between detected
# forest cover loss and the last valid observation
# Creating initial classes for the temporal relationship of fire and forest cover loss based on whether forest
# cover loss was detected within a timespan of 30 days before and after a fire alert
# Initial classes:  1 -     Fire happened before the loss of forest cover
#                   10 -    Fire happened during the loss of forest cover
#                   100 -   Fire happened after the loss of forest cover
#                   1000 -  Forest cover loss was detected

for (i in 1:length(rows)) {
  if (i == 1) {
    opt_date =  order((c(lnbr_date, s2nbr_date)))
    opt_stack = stack(lnbr,s2nbr)
    opt_stack = subset(opt_stack, opt_date)
    
    opt_date = vector(mode="character", length=nlayers(opt_stack))
    for (o in 1:nlayers(opt_stack)) {
      opt_date[o] = gsub("Date_","",opt_stack[[o]]@data@names)
    }
    opt_date = round(decimal_date(as.Date(opt_date, format = "%Y%m%d")), digits = 3)
  }
  temp_result = round(as.numeric(fin_matrix[rows[i],7]), digits = 3)
  date_temp = which(opt_date %in% temp_result)
  temp_cell = cellFromRowCol(opt_stack[[1]], row = as.numeric(fin_matrix[rows[i],4]),col = as.numeric(fin_matrix[rows[i],5]))
  temp_cell = which(!is.na(as.vector(opt_stack[temp_cell])))
  temp_cell = temp_cell[which(temp_cell == date_temp)-1]

  fin_matrix[rows[i],7] = format(date_decimal(as.numeric( (opt_date[date_temp] + opt_date[temp_cell]) / 2 )), "%Y-%m-%d")
  
  fire_sequence = na.omit(as.Date(fin_matrix[rows[i],c(8:ncol(fin_matrix))],format = "%Y-%m-%d"))
  fin_matrix[rows[i],3] = 1000
  if (any(fire_sequence >= (as.Date(fin_matrix[rows[i],7], format = "%Y-%m-%d") -30) & fire_sequence <= (as.Date(fin_matrix[rows[i],7], format = "%Y-%m-%d") +30)) == T) {
    fin_matrix[rows[i],3] = as.numeric(fin_matrix[rows[i],3]) + 10
    next
  }
  if (any(fire_sequence < (as.Date(fin_matrix[rows[i],7], format = "%Y-%m-%d") -30)) == T) {
    fin_matrix[rows[i],3] = as.numeric(fin_matrix[rows[i],3]) + 100
  }
  if (any(fire_sequence > (as.Date(fin_matrix[rows[i],7], format = "%Y-%m-%d") +30)) == T) {
    fin_matrix[rows[i],3] = as.numeric(fin_matrix[rows[i],3]) + 1
  }
}

#########################################################################################################
#------------------ 4. Classification of the fire-related forest cover loss archetypes -----------------#
#########################################################################################################
# Converting inital classes into the final archetypes
fin_matrix[(fin_matrix[,3] == "1010" | fin_matrix[,3] == "1110" | fin_matrix[,3] == "1011" | fin_matrix[,3] == "1111")
           & (fin_matrix[,2] == "1100" | fin_matrix[,2] == "1101"),1] = 2
fin_matrix[(fin_matrix[,3] == "1010" | fin_matrix[,3] == "1110" | fin_matrix[,3] == "1011" | fin_matrix[,3] == "1111")
           & fin_matrix[,2] == "1001",1] = 3
fin_matrix[(fin_matrix[,3] == "1010" | fin_matrix[,3] == "1110" | fin_matrix[,3] == "1011" | fin_matrix[,3] == "1111")
           & is.na(fin_matrix[,2]),1] = 4
fin_matrix[is.na(fin_matrix[,2]) & (fin_matrix[,3] == "1101" | fin_matrix[,3] == "1100"),1] = 5
fin_matrix[is.na(fin_matrix[,2]) & (fin_matrix[,3] == "1001"),1] = 6
fin_matrix[(fin_matrix[,2] == "1010" | fin_matrix[,2] == "1110" | fin_matrix[,2] == "1011" | fin_matrix[,2] == "1111")
           & (fin_matrix[,3] == "1100" | fin_matrix[,3] == "1101") ,1] = 7
fin_matrix[(fin_matrix[,2] == "1010" | fin_matrix[,2] == "1110" | fin_matrix[,2] == "1011" | fin_matrix[,2] == "1111")
           & fin_matrix[,3] == "1001",1] = 8
fin_matrix[(fin_matrix[,2] == "1010" | fin_matrix[,2] == "1110" | fin_matrix[,2] == "1011" | fin_matrix[,2] == "1111")
           & is.na(fin_matrix[,3]),1] = 9
fin_matrix[is.na(fin_matrix[,3]) & (fin_matrix[,2] == "1101" | fin_matrix[,2] == "1100"),1] = 10
fin_matrix[is.na(fin_matrix[,3]) & (fin_matrix[,2] == "1001"),1] = 11
fin_matrix[(fin_matrix[,2] == "1101" | fin_matrix[,2] == "1100") & (fin_matrix[,3] == "1101" | fin_matrix[,3] == "1100"),1] = 12
fin_matrix[fin_matrix[,2] == "1001" & fin_matrix[,3] == "1001",1] = 13
fin_matrix[fin_matrix[,2] == "1000" | fin_matrix[,3] == "1000",1] = 14
fin_matrix[(fin_matrix[,2] == "1001" & fin_matrix[,3] == "1100") | (fin_matrix[,2] == "1100" & fin_matrix[,3] == "1001"),1] = 15
fin_matrix[(is.na(fin_matrix[,1])) & (!is.na(fin_matrix[,2])  | !is.na(fin_matrix[,3])),1] = 15
fin_matrix[(fin_matrix[,2] == "1010" | fin_matrix[,2] == "1110" | fin_matrix[,2] == "1011" | fin_matrix[,2] == "1111")
           & (fin_matrix[,3] == "1010" | fin_matrix[,3] == "1110" | fin_matrix[,3] == "1011" | fin_matrix[,3] == "1111"),1] = 1

fin_matrix[fin_matrix[,1] == "1",1] = 1
fin_matrix[fin_matrix[,1] == "2",1] = 2
fin_matrix[fin_matrix[,1] == "3",1] = NA
fin_matrix[fin_matrix[,1] == "4",1] = 1
fin_matrix[fin_matrix[,1] == "5",1] = 3
fin_matrix[fin_matrix[,1] == "6",1] = 5
fin_matrix[fin_matrix[,1] == "7",1] = 1
fin_matrix[fin_matrix[,1] == "8",1] = 4
fin_matrix[fin_matrix[,1] == "9",1] = 1
fin_matrix[fin_matrix[,1] == "10",1] = 2
fin_matrix[fin_matrix[,1] == "11",1] = 7
fin_matrix[fin_matrix[,1] == "12",1] = 5
fin_matrix[fin_matrix[,1] == "13",1] = 6
fin_matrix[fin_matrix[,1] == "14",1] = 7
fin_matrix[fin_matrix[,1] == "15",1] = NA


#########################################################################################################
#------------ 5. Storing and exporting fire-related forest cover loss archetypes as geotif -------------#
#########################################################################################################
fin_conv_matrix = matrix(NA, ncol = ncol(stack_attemp[[1]]) ,nrow = nrow(stack_attemp[[1]]))
for (u in 1:(nrow(fin_matrix))) {
  fin_conv_matrix[as.numeric(fin_matrix[u,4]),as.numeric(fin_matrix[u,5])] = as.numeric(fin_matrix[u,1])
}
ffcl_ras = raster(fin_conv_matrix)
extent(ffcl_ras) <- c(stack_attemp[[1]]@extent@xmin, stack_attemp[[1]]@extent@xmax,stack_attemp[[1]]@extent@ymin, stack_attemp[[1]]@extent@ymax)
crs(ffcl_ras) <- stack_attemp[[1]]@crs
plot(ffcl_ras,main=paste("Fire-related forest cover loss archetypes"))
writeRaster(ffcl_ras, filename = "./02_results/archetypes_ffcl.tif", format = "GTiff" ,overwrite=T)
rm(list=ls())
gc()
