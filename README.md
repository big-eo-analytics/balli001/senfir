# SENFIR version 0.2 (30-06-2020)                                                         

Author: [Johannes Balling (Wageningen University)](https://www.wur.nl/de/Personen/Johannes-J-Johannes-Balling-MSc.htm)                                          

## Description
Enables the opportunity to classify so called fire related forest cover loss archetypes utilizing multi-sensor data streams (namely: [Sentinel-1](https://sentinel.esa.int/web/sentinel/missions/sentinel-1), [Sentinel-2](https://sentinel.esa.int/web/sentinel/missions/sentinel-2), [Landsat](https://landsat.gsfc.nasa.gov/data/) and active fire alerts based on [MODIS and VIIRs](https://earthdata.nasa.gov/earth-observation-data/near-real-time/firms/active-fire-data) sensor)                                                         

This version of the algorithm includes (for a detailed workflow see figure 1): 

1. Detection of forest cover loss of optical (I) and radar (II) data based on the bayts package developed by [Reiche et al 2015](https://www.mdpi.com/2072-4292/7/5/4973) and [Reiche et al 2018](https://www.sciencedirect.com/science/article/pii/S0034425717304959?via%3Dihub). Please check the [Github page of Bayts](https://github.com/jreiche/bayts) to gain more insights of the forest cover loss detection.

2. Calculation of the uncertainties of the detected forest cover losses

3. Intersection of detected forest cover losses and active fire alerts

4. Classification of 7 archetypes taking the dates of the detected forest cover losses of the optical and radar data respectively into account in respect to the date of fire
                                                                                      
Note: As of this version all raster stacks need to have the same extent and spatial resolution! Moreover, dates of the raster layers must be included as names of the respected raster layer.

![Figure 1: Workflow SENFIR](figures/senfir_workflow.PNG "Workflow SENFIR")
*Figure 1: Workflow SENFIR*

## Example
Please download and install [R Version 3.4.4](https://cran.r-project.org/bin/windows/base/old/3.4.4/) to execute the example code as it was developed and tested for this version. 

The [example code](https://gitlab.com/big-eo-analytics/balli001/senfir/-/blob/master/SENFIR_V_0_2_script.R) calculates fire-related forest cover loss archetypes for a small region in Riau (Indonesia).

The data and code necessary can be downloaded [here](https://sharepoint.wur.nl/sites/big_eo_analytics/Shared%20Documents/Delivarables/SENFIR_v_0_2.zip).

To explore the functionality of SENFIR you have to execute the R code called "SENFIR_V_01_script.R" in the previous installed R software version 3.4.4.
Only Line 23 of the example code needs to be change to the path of the downloaded "SENFIR" folder. Please follow the instructions in the code.

The output of the algorithm is:

1. "radar_forest_cover_loss.tif" - Raster of the dates of the detected forest cover loss utilizing radar data

2. "optical_forest_cover_loss.tif" - Raster of the dates of the detected forest cover loss utilizing optical data

3. "archetypes_ffcl.tif" - Raster of the different archetypes of fire-related forest cover loss (see figure 2).

![Figure 2: SENFIR - Fire-related archetypes SENFIR](figures/senfir_archetypes.PNG "Workflow SENFIR")

*Figure 2: SENFIR - Fire-related archetypes*

### Class description
The definition of the fire-related forest cover loss archetypes as resulting from the algorithm are as follows:

		1 = Loss of tree crown and structure during the fire

		2 = Loss of tree crown during fire; structure after

		3 = Loss of tree crown after fire, intact structure

		4 = Loss of structure during fire; tree crown loss before

		5 = Loss of tree crown and structure before fire

		6 = Loss of tree crown and structure after fire

		7 = Loss of tree crown and structure without fire

Note: For saving computation power results are included in the downloaded files! 